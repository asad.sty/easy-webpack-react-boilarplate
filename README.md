# Easy webpack react boilerplate

A ready to use easy webpack react boilerplate.

## Instructions

1.  Clone this repo
2.  Run `npm install`
3.  Run `npm start`, **localhost:8080** will open up in your default browser

**If you prefer to install things yourself you can follow the instructions below**

1.  Run `npm init` and type your answers to the questions or you can run `npm init -y` to say yes to every question - you will get default settings
2.  Install the following dependencies:
```
npm i react react-dom -S
```
3.  Install the following dev dependencies:
```
npm i @babel/core @babel/preset-env @babel/preset-react @babel/register babel-core babel-loader babel-preset-env babel-preset-react css-loader html-webpack-plugin style-loader webpack webpack-cli webpack-dev-server -D
```
4. Update your scripts to the following:
```
"start": "webpack-dev-server --mode development --open",
"build": "webpack --mode production"
```
5. Create **.babelrc** file with the following configurations:
```
{
   "presets": ["@babel/preset-env","@babel/preset-react"]
}
```
6. Create **webpack.config.js** file with the following configurations:
```
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              localIdentName: '[local]',
            },
          }
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: './index.html'
    })
  ]
};
```
